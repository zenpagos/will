#!make

wire:
	wire ./container

load_fixtures:
	go run scripts/fixtures/fixtures.go

run:
	docker-compose up --build $(arg)

deploy-locally:
	scripts/check-commands.sh
	scripts/deploy-locally.sh

remove-local-deployment:
	scripts/check-commands.sh
	scripts/remove-local-deployment.sh

gen-proto:
	protoc -I proto/v1 proto/v1/will.proto --go_out=plugins=grpc:proto/v1
	python -m grpc_tools.protoc -I proto/v1 --python_out=proto/v1 --grpc_python_out=proto/v1 proto/v1/will.proto

test:
	go test ./...

fmt:
	go fmt ./...