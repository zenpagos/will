package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/grpc-go/will"
	"gitlab.com/zenpagos/will/config"
	"gitlab.com/zenpagos/will/container"
	"gitlab.com/zenpagos/will/interface/igrpc"
	"gitlab.com/zenpagos/will/repository/mysqlrepo"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health/grpc_health_v1"
	"net"
)

var log = logrus.New()

func init() {
	log.SetReportCaller(true)
	log.SetLevel(logrus.InfoLevel)
	log.SetFormatter(&logrus.JSONFormatter{
		PrettyPrint: true,
	})
}

func main() {
	// read configuration
	conf, err := config.ReadConfig(log)
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf, log)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}

	addr := fmt.Sprintf(":%d", conf.Service.Port)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Panicf("failed to listen: %v", err)
	}

	serviceContainer := container.InitializeContainer(db, conf, log)
	gRPCServer := grpc.NewServer()

	willService := igrpc.NewGRPCServer(log, serviceContainer)
	grpc_will_v1.RegisterWillServiceServer(gRPCServer, willService)

	healthService := igrpc.NewHealthChecker()
	grpc_health_v1.RegisterHealthServer(gRPCServer, healthService)

	if err := gRPCServer.Serve(lis); err != nil {
		log.Panicf("failed to serve: %v", err)
	}
}
