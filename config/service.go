package config

type ServiceConfiguration struct {
	Port              int     `mapstructure:"port"`
	Prod              bool    `mapstructure:"prod"`
	DefaultMerchantID string  `mapstructure:"default_merchant_id"`
	DefaultTaxRate    float32 `mapstructure:"default_tax_rate"`
}
