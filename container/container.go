package container

import (
	"gitlab.com/zenpagos/will/usecase"
)

type Container struct {
	MerchantUseCase              usecase.MerchantUseCaseInterface
	BrandUseCase                 usecase.BrandUseCaseInterface
	BankUseCase                  usecase.BankUseCaseInterface
	PaymentMethodUseCase         usecase.PaymentMethodUseCaseInterface
	FeeUseCase                   usecase.FeeUseCaseInterface
	AccountUseCase               usecase.AccountUseCaseInterface
	BankAccountUseCase           usecase.BankAccountUseCaseInterface
	PaymentInformationUseCase    usecase.PaymentInformationUseCaseInterface
	CheckoutPaymentMethodUseCase usecase.CheckoutPaymentMethodUseCaseInterface
}

func NewContainer(
	muc usecase.MerchantUseCaseInterface,
	bruc usecase.BrandUseCaseInterface,
	buc usecase.BankUseCaseInterface,
	pmuc usecase.PaymentMethodUseCaseInterface,
	fuc usecase.FeeUseCaseInterface,
	auc usecase.AccountUseCaseInterface,
	bauc usecase.BankAccountUseCaseInterface,
	piuc usecase.PaymentInformationUseCaseInterface,
	cpmuc usecase.CheckoutPaymentMethodUseCaseInterface,
) Container {
	return Container{
		MerchantUseCase:              muc,
		BrandUseCase:                 bruc,
		BankUseCase:                  buc,
		PaymentMethodUseCase:         pmuc,
		FeeUseCase:                   fuc,
		AccountUseCase:               auc,
		BankAccountUseCase:           bauc,
		PaymentInformationUseCase:    piuc,
		CheckoutPaymentMethodUseCase: cpmuc,
	}
}
