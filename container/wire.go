//+build wireinject

package container

import (
	"github.com/google/wire"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/will/config"
	"gitlab.com/zenpagos/will/repository/mysqlrepo"
	"gitlab.com/zenpagos/will/usecase/accountuc"
	"gitlab.com/zenpagos/will/usecase/bankaccountuc"
	"gitlab.com/zenpagos/will/usecase/bankuc"
	"gitlab.com/zenpagos/will/usecase/branduc"
	"gitlab.com/zenpagos/will/usecase/checkoutpmuc"
	"gitlab.com/zenpagos/will/usecase/feeuc"
	"gitlab.com/zenpagos/will/usecase/merchantuc"
	"gitlab.com/zenpagos/will/usecase/payinfouc"
	"gitlab.com/zenpagos/will/usecase/pmuc"
)

func InitializeContainer(db *gorm.DB, conf config.Configuration, log *logrus.Logger) Container {
	wire.Build(
		// Repositories
		mysqlrepo.NewMerchantRepository,
		mysqlrepo.NewBrandRepository,
		mysqlrepo.NewBankRepository,
		mysqlrepo.NewPaymentMethodRepository,
		mysqlrepo.NewFeeRepository,
		mysqlrepo.NewAccountRepository,
		mysqlrepo.NewBankAccountRepository,
		// Use Cases
		merchantuc.NewMerchantUseCase,
		branduc.NewBrandUseCase,
		bankuc.NewBankUseCase,
		pmuc.NewPaymentMethodUseCase,
		feeuc.NewFeeUseCase,
		accountuc.NewAccountUseCase,
		bankaccountuc.NewBankAccountUseCase,
		payinfouc.NewPaymentInformationUseCase,
		checkoutpmuc.NewCheckoutPaymentMethodUseCase,
		NewContainer,
	)
	return Container{}
}
