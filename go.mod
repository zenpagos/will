module gitlab.com/zenpagos/will

go 1.14

require (
	github.com/go-ozzo/ozzo-validation/v4 v4.1.0
	github.com/go-testfixtures/testfixtures/v3 v3.1.2
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/google/wire v0.4.0
	github.com/jinzhu/gorm v1.9.12
	github.com/jnewmano/grpc-json-proxy v0.0.0-20200227201450-d2f9a1e2ec3d
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.5.0
	github.com/spf13/viper v1.6.3
	github.com/stretchr/testify v1.5.1 // indirect
	gitlab.com/zenpagos/grpc-go v0.0.0-20200620195434-de3aaf754e7a
	gitlab.com/zenpagos/tools v0.0.0-20200616225453-f02e1e8b6545
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
	golang.org/x/sys v0.0.0-20200620081246-981b61492c35 // indirect
	golang.org/x/text v0.3.3 // indirect
	google.golang.org/genproto v0.0.0-20200620020550-bd6e04640131 // indirect
	google.golang.org/grpc v1.29.1
)
