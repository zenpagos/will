package igrpc

import (
	"context"
	_ "github.com/jnewmano/grpc-json-proxy/codec"
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/grpc-go/will"
	"gitlab.com/zenpagos/tools"
	"gitlab.com/zenpagos/will/container"
	"gitlab.com/zenpagos/will/model"
)

type gRPCServer struct {
	log       *logrus.Logger
	container container.Container
}

func NewGRPCServer(log *logrus.Logger, c container.Container) grpc_will_v1.WillServiceServer {
	return &gRPCServer{
		log:       log,
		container: c,
	}
}

func (s *gRPCServer) CreateMerchant(
	_ context.Context,
	req *grpc_will_v1.CreateMerchantRequest,
) (*grpc_will_v1.CreateMerchantResponse, error) {

	m := &model.Merchant{
		Name:              req.Name,
		DecidirPublicKey:  req.DecidirPublicKey,
		DecidirPrivateKey: req.DecidirPrivateKey,
		DecidirMerchantID: req.DecidirMerchantID,
		TIN:               req.TIN,
		TINType:           req.TINType,
		Street:            req.Street,
		DoorNumber:        req.DoorNumber,
		ZipCode:           req.ZipCode,
		Category:          req.Category,
		GeographicCode:    req.GeographicCode,
		City:              req.City,
		Province:          req.Province,
		Country:           req.Country,
		Email:             req.Email,
		Phone:             req.Phone,
	}

	if err := s.container.MerchantUseCase.CreateMerchant(m); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	mr := &grpc_will_v1.Merchant{
		ID:                m.ID.String(),
		CreatedAt:         m.CreatedAt,
		Name:              m.Name,
		DecidirPublicKey:  m.DecidirPublicKey,
		DecidirPrivateKey: m.DecidirPrivateKey,
		DecidirMerchantID: m.DecidirMerchantID,
		TIN:               m.TIN,
		TINType:           m.TINType,
		Street:            m.Street,
		DoorNumber:        m.DoorNumber,
		ZipCode:           m.ZipCode,
		Category:          m.Category,
		GeographicCode:    m.GeographicCode,
		City:              m.City,
		Province:          m.Province,
		Country:           m.Country,
		Email:             m.Email,
		Phone:             m.Phone,
	}

	res := &grpc_will_v1.CreateMerchantResponse{
		Merchant: mr,
	}

	return res, nil
}

func (s *gRPCServer) ListMerchants(
	_ context.Context,
	_ *grpc_will_v1.Empty,
) (*grpc_will_v1.ListMerchantsResponse, error) {

	ms, err := s.container.MerchantUseCase.ListMerchants()
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	rms := make([]*grpc_will_v1.Merchant, 0)
	for _, m := range ms {
		rms = append(rms, &grpc_will_v1.Merchant{
			ID:                m.ID.String(),
			CreatedAt:         m.CreatedAt,
			Name:              m.Name,
			DecidirPublicKey:  m.DecidirPublicKey,
			DecidirPrivateKey: m.DecidirPrivateKey,
			DecidirMerchantID: m.DecidirMerchantID,
			TIN:               m.TIN,
			TINType:           m.TINType,
			Street:            m.Street,
			DoorNumber:        m.DoorNumber,
			ZipCode:           m.ZipCode,
			Category:          m.Category,
			GeographicCode:    m.GeographicCode,
			City:              m.City,
			Province:          m.Province,
			Country:           m.Country,
			Email:             m.Email,
			Phone:             m.Phone,
		})
	}

	res := &grpc_will_v1.ListMerchantsResponse{
		Merchants: rms,
	}

	return res, nil
}

func (s *gRPCServer) CreateBankAccount(
	_ context.Context,
	req *grpc_will_v1.CreateBankAccountRequest,
) (*grpc_will_v1.CreateBankAccountResponse, error) {

	ba := &model.BankAccount{
		CBU:         req.CBU,
		Description: req.Description,
		AccountID:   uuid.FromStringOrNil(req.AccountID),
	}

	if err := s.container.BankAccountUseCase.CreateBankAccount(ba); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	rba := &grpc_will_v1.BankAccount{
		ID:          ba.ID.String(),
		CreatedAt:   ba.CreatedAt,
		CBU:         ba.CBU,
		Description: ba.Description,
		AccountID:   ba.AccountID.String(),
	}

	res := &grpc_will_v1.CreateBankAccountResponse{
		BankAccount: rba,
	}

	return res, nil
}

func (s *gRPCServer) ListBankAccounts(
	_ context.Context,
	_ *grpc_will_v1.Empty,
) (*grpc_will_v1.ListBankAccountsResponse, error) {

	bas, err := s.container.BankAccountUseCase.ListBankAccounts()
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	rbas := make([]*grpc_will_v1.BankAccount, 0)
	for _, ba := range bas {
		rbas = append(rbas, &grpc_will_v1.BankAccount{
			ID:          ba.ID.String(),
			CreatedAt:   ba.CreatedAt,
			CBU:         ba.CBU,
			Description: ba.Description,
			AccountID:   ba.AccountID.String(),
		})
	}

	res := &grpc_will_v1.ListBankAccountsResponse{
		BankAccounts: rbas,
	}

	return res, nil
}

func (s *gRPCServer) CreateBrand(
	_ context.Context,
	req *grpc_will_v1.CreateBrandRequest,
) (*grpc_will_v1.CreateBrandResponse, error) {

	b := &model.Brand{
		Name:          req.GetName(),
		CanonicalName: req.GetCanonicalName(),
		LogoURL:       req.GetLogoURL(),
	}

	if err := s.container.BrandUseCase.CreateBrand(b); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	rb := &grpc_will_v1.Brand{
		ID:            b.ID.String(),
		CreatedAt:     b.CreatedAt,
		Name:          b.Name,
		CanonicalName: b.CanonicalName,
		LogoURL:       b.LogoURL,
	}

	res := &grpc_will_v1.CreateBrandResponse{
		Brand: rb,
	}

	return res, nil
}

func (s *gRPCServer) ListBrands(
	_ context.Context,
	_ *grpc_will_v1.Empty,
) (*grpc_will_v1.ListBrandsResponse, error) {
	bs, err := s.container.BrandUseCase.ListBrands()
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	rbs := make([]*grpc_will_v1.Brand, 0)
	for _, b := range bs {
		rbs = append(rbs, &grpc_will_v1.Brand{
			ID:            b.ID.String(),
			CreatedAt:     b.CreatedAt,
			Name:          b.Name,
			CanonicalName: b.CanonicalName,
			LogoURL:       b.LogoURL,
		})
	}

	res := &grpc_will_v1.ListBrandsResponse{
		Brands: rbs,
	}

	return res, nil
}

func (s *gRPCServer) CreateBank(
	_ context.Context,
	req *grpc_will_v1.CreateBankRequest,
) (*grpc_will_v1.CreateBankResponse, error) {

	b := &model.Bank{
		Name:         req.GetName(),
		LogoURL:      req.GetLogoURL(),
		EntityNumber: req.GetEntityNumber(),
	}

	if err := s.container.BankUseCase.CreateBank(b); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	rb := &grpc_will_v1.Bank{
		ID:           b.ID.String(),
		CreatedAt:    b.CreatedAt,
		Name:         b.Name,
		LogoURL:      b.LogoURL,
		EntityNumber: b.EntityNumber,
	}

	res := &grpc_will_v1.CreateBankResponse{
		Bank: rb,
	}

	return res, nil
}

func (s *gRPCServer) ListBanks(
	_ context.Context,
	_ *grpc_will_v1.Empty,
) (*grpc_will_v1.ListBanksResponse, error) {
	bs, err := s.container.BankUseCase.ListBanks()
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	rbs := make([]*grpc_will_v1.Bank, 0)
	for _, b := range bs {
		rbs = append(rbs, &grpc_will_v1.Bank{
			ID:        b.ID.String(),
			CreatedAt: b.CreatedAt,
			Name:      b.Name,
			LogoURL:   b.LogoURL,
		})
	}

	res := &grpc_will_v1.ListBanksResponse{
		Banks: rbs,
	}

	return res, nil
}

func (s *gRPCServer) CreatePaymentMethod(
	_ context.Context,
	req *grpc_will_v1.CreatePaymentMethodRequest,
) (*grpc_will_v1.CreatePaymentMethodResponse, error) {

	pm := &model.PaymentMethod{
		Type:       req.GetType(),
		MerchantID: uuid.FromStringOrNil(req.MerchantID),
		BrandID:    uuid.FromStringOrNil(req.BrandID),
	}

	if err := s.container.PaymentMethodUseCase.CreatePaymentMethod(pm); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	pmr := &grpc_will_v1.PaymentMethod{
		ID:         pm.ID.String(),
		CreatedAt:  pm.CreatedAt,
		Type:       pm.Type,
		MerchantID: pm.MerchantID.String(),
		BrandID:    pm.BrandID.String(),
	}

	res := &grpc_will_v1.CreatePaymentMethodResponse{
		PaymentMethod: pmr,
	}

	return res, nil
}

func (s *gRPCServer) ListPaymentMethods(
	_ context.Context,
	_ *grpc_will_v1.Empty,
) (*grpc_will_v1.ListPaymentMethodsResponse, error) {
	pms, err := s.container.PaymentMethodUseCase.ListPaymentMethods()
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	rpms := make([]*grpc_will_v1.PaymentMethod, 0)
	for _, pm := range pms {
		rpms = append(rpms, &grpc_will_v1.PaymentMethod{
			ID:         pm.ID.String(),
			CreatedAt:  pm.CreatedAt,
			Type:       pm.Type,
			MerchantID: pm.MerchantID.String(),
			BrandID:    pm.BrandID.String(),
		})
	}

	res := &grpc_will_v1.ListPaymentMethodsResponse{
		PaymentMethods: rpms,
	}

	return res, nil
}

func (s *gRPCServer) GetPaymentMethod(
	_ context.Context,
	req *grpc_will_v1.GetPaymentMethodRequest,
) (*grpc_will_v1.GetPaymentMethodResponse, error) {

	id := uuid.FromStringOrNil(req.ID)

	pm, err := s.container.PaymentMethodUseCase.GetPaymentMethod(id)
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	pmr := &grpc_will_v1.PaymentMethod{
		ID:         pm.ID.String(),
		CreatedAt:  pm.CreatedAt,
		Type:       pm.Type,
		MerchantID: pm.MerchantID.String(),
		BrandID:    pm.BrandID.String(),
	}

	res := &grpc_will_v1.GetPaymentMethodResponse{
		PaymentMethod: pmr,
	}

	return res, nil
}

func (s *gRPCServer) GetCheckoutPaymentMethod(
	_ context.Context,
	req *grpc_will_v1.GetCheckoutPaymentMethodRequest,
) (*grpc_will_v1.GetCheckoutPaymentMethodResponse, error) {

	accountID := uuid.FromStringOrNil(req.AccountID)
	amount := req.Amount
	paymentMethod := req.PaymentMethod
	paymentMethodType := req.PaymentMethodType

	pm, err := s.container.PaymentMethodUseCase.GetPaymentMethodForCheckout(accountID, paymentMethod, paymentMethodType)
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	cpm, err := s.container.CheckoutPaymentMethodUseCase.BuildCheckoutPaymentMethod(pm, amount)
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	cis := make([]*grpc_will_v1.CheckoutInstallment, 0)
	for _, i := range cpm.Installments {
		cis = append(cis, &grpc_will_v1.CheckoutInstallment{
			Installments:      i.Installments,
			InstallmentAmount: i.InstallmentAmount,
			AmountToPay:       i.AmountToPay,
		})
	}

	res := &grpc_will_v1.GetCheckoutPaymentMethodResponse{
		Name:                cpm.Name,
		Type:                cpm.Type,
		LogoURL:             cpm.LogoURL,
		CheckoutInstallment: cis,
	}

	return res, nil
}

func (s *gRPCServer) CreateFee(
	_ context.Context,
	req *grpc_will_v1.CreateFeeRequest,
) (*grpc_will_v1.CreateFeeResponse, error) {

	pmf := &model.Fee{
		Description:               req.Description,
		Installments:              req.Installments,
		CardCoefficient:           req.CardCoefficient,
		ProcessingCostFixedAmount: req.FixedSum,
		DaysOnHold:                req.DaysOnHold,
		PaymentMethodID:           uuid.FromStringOrNil(req.PaymentMethodID),
	}

	if err := s.container.FeeUseCase.CreateFee(pmf); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	rf := &grpc_will_v1.Fee{
		ID:              pmf.ID.String(),
		CreatedAt:       pmf.CreatedAt,
		Description:     pmf.Description,
		Installments:    pmf.Installments,
		CardCoefficient: pmf.CardCoefficient,
		FixedSum:        pmf.ProcessingCostFixedAmount,
		DaysOnHold:      pmf.DaysOnHold,
		PaymentMethodID: pmf.PaymentMethodID.String(),
	}

	res := &grpc_will_v1.CreateFeeResponse{
		Fee: rf,
	}

	return res, nil
}

func (s *gRPCServer) ListFees(
	_ context.Context,
	_ *grpc_will_v1.Empty,
) (*grpc_will_v1.ListFeesResponse, error) {
	fs, err := s.container.FeeUseCase.ListFees()
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	rfs := make([]*grpc_will_v1.Fee, 0)
	for _, f := range fs {
		rfs = append(rfs, &grpc_will_v1.Fee{
			ID:              f.ID.String(),
			CreatedAt:       f.CreatedAt,
			Description:     f.Description,
			Installments:    f.Installments,
			CardCoefficient: f.CardCoefficient,
			FixedSum:        f.FeeFixedAmount,
			DaysOnHold:      f.DaysOnHold,
			PaymentMethodID: f.PaymentMethodID.String(),
		})
	}

	res := &grpc_will_v1.ListFeesResponse{
		Fees: rfs,
	}

	return res, nil
}

func (s *gRPCServer) GetInformationToExecuteAPayment(
	_ context.Context,
	req *grpc_will_v1.GetInformationToExecuteAPaymentRequest,
) (*grpc_will_v1.GetInformationToExecuteAPaymentResponse, error) {

	accountID := uuid.FromStringOrNil(req.Account)
	paymentMethodType := req.PaymentMethodType
	paymentMethod := req.PaymentMethod
	installment := req.Installments
	amount := req.Amount

	// get the account
	account, err := s.container.AccountUseCase.GetAccountDetail(accountID)
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	// get the fee
	fee, err := s.container.FeeUseCase.GetFeeToExecuteAPayment(
		accountID,
		paymentMethodType,
		paymentMethod,
		installment,
	)
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	// create a payment information object
	pc, err := s.container.PaymentInformationUseCase.BuildPaymentInformation(account, fee, amount)
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	res := &grpc_will_v1.GetInformationToExecuteAPaymentResponse{
		CardCoefficient:           pc.CardCoefficient,
		ProcessingCostPercentage:  pc.ProcessingCostPercentage,
		ProcessingCostFixedAmount: pc.ProcessingCostFixedAmount,
		FeePercentage:             pc.FeePercentage,
		FeeFixedAmount:            pc.FeeFixedAmount,
		DaysOnHold:                pc.DaysOnHold,
		AmountToPay:               pc.AmountToPay,
		InstallmentAmount:         pc.InstallmentAmount,
		FeeAmount:                 pc.FeeAmount,
		TaxesAmount:               pc.TaxesAmount,
		StatementDescriptor:       pc.StatementDescriptor,
		ReceivedAmount:            pc.ReceivedAmount,
		AmountReleaseDate:         pc.AmountReleaseDate,
		TaxRate:                   pc.TaxRate,
		DecidirPrivateKey:         pc.DecidirPrivateKey,
		MerchantTIN:               pc.MerchantTIN,
		MerchantTINType:           pc.MerchantTINType,
		MerchantName:              pc.MerchantName,
		MerchantStreet:            pc.MerchantStreet,
		MerchantDoorNumber:        pc.MerchantDoorNumber,
		MerchantZipCode:           pc.MerchantZipCode,
		MerchantCategory:          pc.MerchantCategory,
		MerchantGeographicCode:    pc.MerchantGeographicCode,
		MerchantCity:              pc.MerchantCity,
		MerchantID:                pc.MerchantID,
		MerchantProvince:          pc.MerchantProvince,
		MerchantCountry:           pc.MerchantCountry,
		MerchantEmail:             pc.MerchantEmail,
		MerchantPhone:             pc.MerchantPhone,
	}

	return res, nil
}

func (s *gRPCServer) CreateAccount(
	_ context.Context,
	req *grpc_will_v1.CreateAccountRequest,
) (*grpc_will_v1.CreateAccountResponse, error) {

	a := &model.Account{
		Name:                req.Name,
		TIN:                 req.TIN,
		SupportEmail:        tools.StringPointerOrNil(req.SupportEmail),
		SupportPhoneNumber:  tools.StringPointerOrNil(req.SupportPhoneNumber),
		StatementDescriptor: tools.StringPointerOrNil(req.StatementDescriptor),
		UserID:              uuid.FromStringOrNil(req.UserID),
	}

	if err := s.container.AccountUseCase.CreateAccount(a); err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	ar := &grpc_will_v1.Account{
		ID:                  a.ID.String(),
		CreatedAt:           a.CreatedAt,
		UpdatedAt:           tools.Int64ValueOrZero(a.UpdatedAt),
		Name:                a.Name,
		TIN:                 a.TIN,
		SupportEmail:        tools.StringValueOrBlank(a.SupportEmail),
		SupportPhoneNumber:  tools.StringValueOrBlank(a.SupportPhoneNumber),
		StatementDescriptor: tools.StringValueOrBlank(a.StatementDescriptor),
		UserID:              a.UserID.String(),
		MerchantID:          a.MerchantID.String(),
	}

	res := &grpc_will_v1.CreateAccountResponse{
		Account: ar,
	}

	return res, nil
}

func (s *gRPCServer) ListAccounts(
	_ context.Context,
	_ *grpc_will_v1.Empty,
) (*grpc_will_v1.ListAccountsResponse, error) {

	as, err := s.container.AccountUseCase.ListAccounts()
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	ras := make([]*grpc_will_v1.Account, 0)
	for _, a := range as {
		ras = append(ras, &grpc_will_v1.Account{
			ID:                  a.ID.String(),
			CreatedAt:           a.CreatedAt,
			UpdatedAt:           tools.Int64ValueOrZero(a.UpdatedAt),
			Name:                a.Name,
			TIN:                 a.TIN,
			SupportEmail:        tools.StringValueOrBlank(a.SupportEmail),
			SupportPhoneNumber:  tools.StringValueOrBlank(a.SupportPhoneNumber),
			StatementDescriptor: tools.StringValueOrBlank(a.StatementDescriptor),
			UserID:              a.UserID.String(),
			MerchantID:          a.MerchantID.String(),
		})
	}

	res := &grpc_will_v1.ListAccountsResponse{
		Accounts: ras,
	}

	return res, nil
}

func (s *gRPCServer) GetAccountDetail(
	_ context.Context,
	req *grpc_will_v1.GetAccountDetailRequest,
) (*grpc_will_v1.GetAccountDetailResponse, error) {
	accountID := uuid.FromStringOrNil(req.ID)

	a, err := s.container.AccountUseCase.GetAccountDetail(accountID)
	if err != nil {
		s.log.WithError(err).Error()
		return nil, err
	}

	ar := &grpc_will_v1.Account{
		ID:                  a.ID.String(),
		CreatedAt:           a.CreatedAt,
		UpdatedAt:           tools.Int64ValueOrZero(a.UpdatedAt),
		Name:                a.Name,
		TIN:                 a.TIN,
		SupportEmail:        tools.StringValueOrBlank(a.SupportEmail),
		SupportPhoneNumber:  tools.StringValueOrBlank(a.SupportPhoneNumber),
		StatementDescriptor: tools.StringValueOrBlank(a.StatementDescriptor),
		UserID:              a.UserID.String(),
		MerchantID:          a.MerchantID.String(),
	}

	res := &grpc_will_v1.GetAccountDetailResponse{
		Account: ar,
	}

	return res, nil
}
