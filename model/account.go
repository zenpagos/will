// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"github.com/go-ozzo/ozzo-validation/v4/is"
	uuid "github.com/satori/go.uuid"
)

type Account struct {
	ID                  uuid.UUID `sql:"type:varchar(255);primary_key;"`
	CreatedAt           int64     `sql:"type:int;not null"`
	UpdatedAt           *int64    `sql:"type:int"`
	Name                string    `sql:"type:varchar(255);not null"`
	TIN                 string    `sql:"type:varchar(12);not null"`
	SupportEmail        *string   `sql:"type:varchar(255)"`
	SupportPhoneNumber  *string   `sql:"type:varchar(255)"`
	StatementDescriptor *string   `sql:"type:varchar(25)"`
	UserID              uuid.UUID `sql:"type:varchar(255);not null"`
	Merchant            Merchant
	MerchantID          uuid.UUID `sql:"type:varchar(255);not null"`
}

func (a Account) Validate() error {
	return validation.ValidateStruct(&a,
		validation.Field(&a.Name, validation.Required),
		validation.Field(&a.TIN, validation.Required),
		validation.Field(&a.UserID, is.UUIDv4, validation.Required),
	)
}
