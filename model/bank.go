// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	uuid "github.com/satori/go.uuid"
)

type Bank struct {
	ID           uuid.UUID `sql:"type:varchar(255);primary_key;"`
	CreatedAt    int64     `sql:"type:int;not null"`
	UpdatedAt    *int64    `sql:"type:int"`
	Name         string    `sql:"type:varchar(255)"`
	LogoURL      string    `sql:"type:varchar(255)"`
	EntityNumber int64     `sql:"type:int"`
}

func (b Bank) Validate() error {
	return validation.ValidateStruct(&b,
		validation.Field(&b.Name, validation.Required),
		validation.Field(&b.LogoURL, validation.Required),
		validation.Field(&b.EntityNumber, validation.Required),
	)
}

func (b *Bank) Complete() error {
	return nil
}
