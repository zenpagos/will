package model

type CheckoutInstallment struct {
	Installments      int64
	InstallmentAmount int64
	AmountToPay       int64
}
