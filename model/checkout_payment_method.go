package model

type CheckoutPaymentMethod struct {
	Name         string
	Type         string
	LogoURL      string
	Installments []*CheckoutInstallment
}
