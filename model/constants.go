package model

const (
	ACHPaymentMethodType        = "ach"
	CashPaymentMethodType       = "cash"
	CreditCardPaymentMethodType = "credit.card"
	DebitCardPaymentMethodType  = "debit.card"
	PEIPaymentMethodType        = "pei"
	VEPPaymentMethodType        = "vep"
	PlatformMethodType          = "platform"

	CUITTINType = "cuit"
	CUILTINType = "cuil"
	DUTINType   = "du"
)
