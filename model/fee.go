// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	uuid "github.com/satori/go.uuid"
)

type Fee struct {
	ID           uuid.UUID `sql:"type:varchar(255);primary_key;"`
	CreatedAt    int64     `sql:"type:int;not null"`
	UpdatedAt    *int64    `sql:"type:int"`
	Description  string    `sql:"type:varchar(255)"`
	Installments int64     `sql:"type:int;not null"`

	// this field is used to calculate the final amount that a payer
	// has to pay to choose to make the payment in installments
	CardCoefficient float32 `sql:"type:decimal(14,4);not null"`

	// these fields are used to calculate the
	// processing cost and determine the revenue
	ProcessingCostPercentage  float32 `sql:"type:decimal(5,2);not null"`
	ProcessingCostFixedAmount int64   `sql:"type:int;not null"`

	// these fields are used to apply fees to the account owner
	FeePercentage  float32 `sql:"type:decimal(5,2);not null"`
	FeeFixedAmount int64   `sql:"type:int;not null"`

	DaysOnHold      int64 `sql:"type:int;not null"`
	PaymentMethod   PaymentMethod
	PaymentMethodID uuid.UUID `sql:"type:varchar(255);not null"`
}

func (f Fee) Validate() error {
	return validation.ValidateStruct(&f,
		validation.Field(&f.Description, validation.Required),
		validation.Field(&f.Installments, validation.Required),
		validation.Field(&f.CardCoefficient, validation.Required),
		validation.Field(&f.DaysOnHold, validation.Required),
		validation.Field(&f.PaymentMethodID, externalUUIDRule...),
	)
}

func (f *Fee) Complete() error {
	return nil
}
