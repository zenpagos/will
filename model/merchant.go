// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	uuid "github.com/satori/go.uuid"
)

type Merchant struct {
	ID        uuid.UUID `sql:"type:varchar(255);primary_key;"` // autogenerated
	CreatedAt int64     `sql:"type:int;not null"`              // autogenerated
	UpdatedAt *int64    `sql:"type:int"`                       // autogenerated
	Name      string    `sql:"type:varchar(255)"`

	// decidir credentials information
	DecidirPublicKey  string `sql:"type:varchar(255);not null"`
	DecidirPrivateKey string `sql:"type:varchar(255);not null"`

	// merchant information needed to process payments through Decidir
	// as aggregator commerce. These fields are used in the request
	// to execute a payment in the `aggregate_data` block
	DecidirMerchantID string `sql:"type:varchar(255);not null"`
	TIN               string `sql:"type:varchar(255);not null"`
	TINType           string `sql:"type:varchar(255);not null"`
	Street            string `sql:"type:varchar(255);not null"`
	DoorNumber        string `sql:"type:varchar(255);not null"`
	ZipCode           string `sql:"type:varchar(255);not null"`
	Category          string `sql:"type:varchar(255);not null"`
	GeographicCode    string `sql:"type:varchar(255);not null"`
	City              string `sql:"type:varchar(255);not null"`
	Province          string `sql:"type:varchar(255);not null"`
	Country           string `sql:"type:varchar(255);not null"`
	Email             string `sql:"type:varchar(255);not null"`
	Phone             string `sql:"type:varchar(255);not null"`

	PaymentMethods []PaymentMethod
}

func (m Merchant) Validate() error {
	return validation.ValidateStruct(&m,
		validation.Field(&m.Name, validation.Required),

		// decidir credentials information
		validation.Field(&m.DecidirPublicKey, validation.Required),
		validation.Field(&m.DecidirPrivateKey, validation.Required),

		// merchant information
		validation.Field(&m.DecidirMerchantID, validation.Required),
		validation.Field(&m.TIN, validation.Required),
		validation.Field(&m.TINType, tinTypesRule...),
		validation.Field(&m.Name, validation.Required),
		validation.Field(&m.Street, validation.Required),
		validation.Field(&m.DoorNumber, validation.Required),
		validation.Field(&m.ZipCode, validation.Required),
		validation.Field(&m.Category, validation.Required),
		validation.Field(&m.GeographicCode, validation.Required),
		validation.Field(&m.City, validation.Required),
		validation.Field(&m.Province, validation.Required),
		validation.Field(&m.Country, validation.Required),
		validation.Field(&m.Email, validation.Required),
		validation.Field(&m.Phone, validation.Required),
	)
}
