// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

type PaymentInformation struct {
	Description               string
	CardCoefficient           float32
	ProcessingCostPercentage  float32
	ProcessingCostFixedAmount int64
	FeePercentage             float32
	FeeFixedAmount            int64
	DaysOnHold                int64
	Installments              int64
	Amount                    int64
	TaxRate                   float32
	StatementDescriptor       string

	// decidir information
	DecidirPrivateKey string

	// merchant information
	MerchantTIN            string
	MerchantTINType        string
	MerchantName           string
	MerchantStreet         string
	MerchantDoorNumber     string
	MerchantZipCode        string
	MerchantCategory       string
	MerchantGeographicCode string
	MerchantCity           string
	MerchantID             string
	MerchantProvince       string
	MerchantCountry        string
	MerchantEmail          string
	MerchantPhone          string

	// these fields are calculated with the fields above
	AmountToPay       int64
	InstallmentAmount int64
	FeeAmount         int64
	TaxesAmount       int64
	ReceivedAmount    int64
	AmountReleaseDate int64
}
