// Package model represents domain model. Every domain model type should have it's own file.
// It shouldn't depends on any other package in the application.
// It should only has domain model type and limited domain logic, in this example, validation logic. Because all other
// package depends on this package, the import of this package should be as small as possible.
package model

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	uuid "github.com/satori/go.uuid"
)

type PaymentMethod struct {
	ID         uuid.UUID `sql:"type:varchar(255);primary_key;"`
	CreatedAt  int64     `sql:"type:int;not null"`
	UpdatedAt  *int64    `sql:"type:int"`
	Type       string    `sql:"type:varchar(255)"`
	Merchant   Merchant
	MerchantID uuid.UUID `sql:"type:varchar(255);not null"`
	Brand      Brand
	BrandID    uuid.UUID `sql:"type:varchar(255);not null"`
	Fees       []Fee
}

func (pm PaymentMethod) Validate() error {
	return validation.ValidateStruct(&pm,
		validation.Field(&pm.Type, paymentMethodTypeRule...),
		validation.Field(&pm.MerchantID, externalUUIDRule...),
		validation.Field(&pm.BrandID, externalUUIDRule...),
	)
}
