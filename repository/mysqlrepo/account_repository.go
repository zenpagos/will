package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
)

type AccountRepository struct {
	DB *gorm.DB
}

func NewAccountRepository(db *gorm.DB) repository.AccountRepositoryInterface {
	return AccountRepository{
		DB: db,
	}
}

func (r AccountRepository) Insert(a *model.Account) error {
	return r.DB.Create(&a).Error
}

func (r AccountRepository) FindAll() ([]model.Account, error) {
	accounts := make([]model.Account, 0)

	if err := r.DB.Find(&accounts).Error; err != nil {
		return nil, err
	}

	return accounts, nil
}

func (r AccountRepository) FindOne(id uuid.UUID) (*model.Account, error) {
	account := new(model.Account)
	if err := r.DB.Where("ID = ?", id.String()).First(account).Error; err != nil {
		return nil, err
	}

	return account, nil
}
