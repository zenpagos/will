package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
)

type BankAccountRepository struct {
	DB *gorm.DB
}

func NewBankAccountRepository(db *gorm.DB) repository.BankAccountRepositoryInterface {
	return BankAccountRepository{
		DB: db,
	}
}

func (r BankAccountRepository) Insert(ba *model.BankAccount) error {
	return r.DB.Create(&ba).Error
}

func (r BankAccountRepository) FindAll() ([]model.BankAccount, error) {
	bankAccounts := make([]model.BankAccount, 0)

	if err := r.DB.Find(&bankAccounts).Error; err != nil {
		return nil, err
	}

	return bankAccounts, nil
}
