package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
)

type BankRepository struct {
	DB *gorm.DB
}

func NewBankRepository(db *gorm.DB) repository.BankRepositoryInterface {
	return BankRepository{
		DB: db,
	}
}

func (r BankRepository) Insert(b *model.Bank) error {
	return r.DB.Create(&b).Error
}

func (r BankRepository) FindAll() ([]model.Bank, error) {
	banks := make([]model.Bank, 0)

	if err := r.DB.Find(&banks).Error; err != nil {
		return nil, err
	}

	return banks, nil
}
