package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
)

type BrandRepository struct {
	DB *gorm.DB
}

func NewBrandRepository(db *gorm.DB) repository.BrandRepositoryInterface {
	return BrandRepository{
		DB: db,
	}
}

func (r BrandRepository) Insert(b *model.Brand) error {
	return r.DB.Create(&b).Error
}

func (r BrandRepository) FindAll() ([]model.Brand, error) {
	brands := make([]model.Brand, 0)

	if err := r.DB.Find(&brands).Error; err != nil {
		return nil, err
	}

	return brands, nil
}
