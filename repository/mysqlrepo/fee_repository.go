package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
)

type FeeRepository struct {
	DB *gorm.DB
}

func NewFeeRepository(db *gorm.DB) repository.FeeRepositoryInterface {
	return FeeRepository{
		DB: db,
	}
}

func (r FeeRepository) Insert(pmf *model.Fee) error {
	return r.DB.Create(&pmf).Error
}

func (r FeeRepository) FindAll() ([]model.Fee, error) {
	fees := make([]model.Fee, 0)

	if err := r.DB.Find(&fees).Error; err != nil {
		return nil, err
	}

	return fees, nil
}

func (r FeeRepository) FindOneByPaymentInformation(
	accountID uuid.UUID,
	paymentMethodType string,
	paymentMethod string,
	installments int64,
) (*model.Fee, error) {

	pmf := new(model.Fee)
	j1 := "JOIN payment_methods ON payment_methods.id = fees.payment_method_id AND payment_methods.type = ?"
	j2 := "JOIN brands ON brands.id = payment_methods.brand_id AND brands.canonical_name = ?"
	j3 := "JOIN merchants ON merchants.id = payment_methods.merchant_id"
	j4 := "JOIN accounts ON accounts.merchant_id = merchants.id AND accounts.id = ?"
	q := "fees.installments = ?"

	if err := r.DB.
		Preload("PaymentMethod.Merchant").
		Joins(j1, paymentMethodType).
		Joins(j2, paymentMethod).
		Joins(j3).
		Joins(j4, accountID.String()).
		Where(q, installments).
		First(pmf).Error; err != nil {
		return nil, err
	}

	return pmf, nil
}
