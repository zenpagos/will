package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
)

type MerchantRepository struct {
	DB *gorm.DB
}

func NewMerchantRepository(db *gorm.DB) repository.MerchantRepositoryInterface {
	return MerchantRepository{
		DB: db,
	}
}

func (r MerchantRepository) Insert(cs *model.Merchant) error {
	return r.DB.Create(&cs).Error
}

func (r MerchantRepository) FindAll() ([]model.Merchant, error) {
	merchants := make([]model.Merchant, 0)

	if err := r.DB.Find(&merchants).Error; err != nil {
		return nil, err
	}

	return merchants, nil
}
