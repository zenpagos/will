package mysqlrepo

import (
	"github.com/jinzhu/gorm"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
)

type PaymentMethodRepository struct {
	DB *gorm.DB
}

func NewPaymentMethodRepository(db *gorm.DB) repository.PaymentMethodRepositoryInterface {
	return PaymentMethodRepository{
		DB: db,
	}
}

func (r PaymentMethodRepository) FindOne(id uuid.UUID) (*model.PaymentMethod, error) {
	pm := new(model.PaymentMethod)
	if err := r.DB.Where("ID = ?", id).First(pm).Error; err != nil {
		return nil, err
	}

	return pm, nil
}

func (r PaymentMethodRepository) Insert(pm *model.PaymentMethod) error {
	return r.DB.Create(&pm).Error
}

func (r PaymentMethodRepository) FindAll() ([]model.PaymentMethod, error) {
	paymentMethods := make([]model.PaymentMethod, 0)

	if err := r.DB.Find(&paymentMethods).Error; err != nil {
		return nil, err
	}

	return paymentMethods, nil
}

func (r PaymentMethodRepository) FindOneByAccountIDNameAndType(
	accountID uuid.UUID,
	paymentMethod string,
	paymentMethodType string,
) (*model.PaymentMethod, error) {

	pm := new(model.PaymentMethod)
	j1 := "JOIN brands ON brands.id = payment_methods.brand_id AND brands.canonical_name = ?"
	j2 := "JOIN merchants ON merchants.id = payment_methods.merchant_id"
	j3 := "JOIN accounts ON accounts.merchant_id = merchants.id AND accounts.id = ?"
	q := "payment_methods.type = ?"

	if err := r.DB.
		Preload("Brand").
		Preload("Fees", func(db *gorm.DB) *gorm.DB {
			return db.Order("fees.installments ASC")
		}).
		Joins(j1, paymentMethod).
		Joins(j2).
		Joins(j3, accountID.String()).
		Where(q, paymentMethodType).
		First(pm).Error; err != nil {
		return nil, err
	}

	return pm, nil
}
