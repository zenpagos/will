// Package repository and it's sub-package represents data persistence service, mainly access database,
// but also including data persisted by other Micro-service.
// For Micro-server, only the interface is defined in this package, the data transformation code is in adapter package
// This is the top level package and it only defines interface, and all implementations are defined in sub-package
// Use case package depends on it.
package repository

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/will/model"
)

type MerchantRepositoryInterface interface {
	Insert(merchant *model.Merchant) error
	FindAll() ([]model.Merchant, error)
}

type BrandRepositoryInterface interface {
	Insert(brand *model.Brand) error
	FindAll() ([]model.Brand, error)
}

type BankRepositoryInterface interface {
	Insert(bank *model.Bank) error
	FindAll() ([]model.Bank, error)
}

type PaymentMethodRepositoryInterface interface {
	Insert(paymentMethod *model.PaymentMethod) error
	FindAll() ([]model.PaymentMethod, error)
	FindOne(id uuid.UUID) (*model.PaymentMethod, error)
	FindOneByAccountIDNameAndType(
		accountID uuid.UUID,
		paymentMethod string,
		paymentMethodType string,
	) (*model.PaymentMethod, error)
}

type FeeRepositoryInterface interface {
	Insert(fee *model.Fee) error
	FindAll() ([]model.Fee, error)
	FindOneByPaymentInformation(
		accountID uuid.UUID,
		paymentMethodType string,
		paymentMethod string,
		installments int64,
	) (*model.Fee, error)
}

type AccountRepositoryInterface interface {
	Insert(account *model.Account) error
	FindAll() ([]model.Account, error)
	FindOne(id uuid.UUID) (*model.Account, error)
}

type BankAccountRepositoryInterface interface {
	Insert(bankAccount *model.BankAccount) error
	FindAll() ([]model.BankAccount, error)
}
