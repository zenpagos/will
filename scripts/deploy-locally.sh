#!/bin/bash

SVC_DEPLOYMENT=will

docker build -t 127.0.0.1:5000/will .

docker push 127.0.0.1:5000/will

helm upgrade --install \
  -f scripts/will-values.yaml \
  ${SVC_DEPLOYMENT} kubernetes/will
