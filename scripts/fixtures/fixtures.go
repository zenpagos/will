package main

import (
	"github.com/go-testfixtures/testfixtures/v3"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/will/config"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository/mysqlrepo"
)

var log = logrus.New()

func init() {
	log.SetReportCaller(true)
	log.SetLevel(logrus.InfoLevel)
	log.SetFormatter(&logrus.JSONFormatter{
		PrettyPrint: true,
	})
}

func main() {
	// read configuration
	conf, err := config.ReadConfig(log)
	if err != nil {
		log.Panicf("error getting configuration params: %v", err)
	}

	if conf.Service.Prod == false {
		log.SetLevel(logrus.TraceLevel)
	}

	// connect to database
	db, err := mysqlrepo.NewMysqlClient(conf, log)
	if err != nil {
		log.Panicf("failed to connect to mysql: %v", err)
	}
	defer db.Close()

	db.DropTableIfExists(
		&model.Merchant{},
		&model.Brand{},
		&model.Bank{},
		&model.PaymentMethod{},
		&model.Fee{},
		&model.Account{},
		&model.BankAccount{},
	)

	db.AutoMigrate(
		&model.Merchant{},
		&model.Brand{},
		&model.Bank{},
		&model.PaymentMethod{},
		&model.Fee{},
		&model.Account{},
		&model.BankAccount{},
	)

	fixtures, err := testfixtures.New(
		testfixtures.Database(db.DB()),
		testfixtures.Dialect(mysqlrepo.DatabaseDialect),
		testfixtures.DangerousSkipTestDatabaseCheck(),
		testfixtures.Directory("fixtures"),
	)
	if err != nil {
		log.Panicf("failed to create fixtures: %v", err)
	}

	if err := fixtures.Load(); err != nil {
		log.Panicf("failed to load fixtures: %v", err)
	}
}
