// Package registration represents the concrete implementation of AccountCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package accountuc

import (
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/will/config"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
	"gitlab.com/zenpagos/will/usecase"
)

type AccountUseCase struct {
	log               *logrus.Logger
	Configuration     config.Configuration
	AccountRepository repository.AccountRepositoryInterface
}

func NewAccountUseCase(
	log *logrus.Logger,
	conf config.Configuration,
	ar repository.AccountRepositoryInterface,
) usecase.AccountUseCaseInterface {
	return AccountUseCase{
		log:               log,
		Configuration:     conf,
		AccountRepository: ar,
	}
}

func (uc AccountUseCase) CreateAccount(a *model.Account) error {
	if err := a.Validate(); err != nil {
		return err
	}

	// getting merchantID from configuration
	merchantID, err := uuid.FromString(uc.Configuration.Service.DefaultMerchantID)
	if err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	a.MerchantID = merchantID

	if err := uc.AccountRepository.Insert(a); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc AccountUseCase) ListAccounts() ([]model.Account, error) {
	return uc.AccountRepository.FindAll()
}

func (uc AccountUseCase) GetAccountDetail(id uuid.UUID) (*model.Account, error) {
	return uc.AccountRepository.FindOne(id)
}
