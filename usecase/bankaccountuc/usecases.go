// Package registration represents the concrete implementation of BankAccountCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package bankaccountuc

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
	"gitlab.com/zenpagos/will/usecase"
)

type BankAccountUseCase struct {
	log                   *logrus.Logger
	BankAccountRepository repository.BankAccountRepositoryInterface
}

func NewBankAccountUseCase(
	log *logrus.Logger,
	ar repository.BankAccountRepositoryInterface,
) usecase.BankAccountUseCaseInterface {
	return BankAccountUseCase{
		log:                   log,
		BankAccountRepository: ar,
	}
}

func (uc BankAccountUseCase) CreateBankAccount(ba *model.BankAccount) error {
	if err := ba.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := ba.Complete(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := uc.BankAccountRepository.Insert(ba); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc BankAccountUseCase) ListBankAccounts() ([]model.BankAccount, error) {
	return uc.BankAccountRepository.FindAll()
}
