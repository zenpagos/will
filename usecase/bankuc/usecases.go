// Package registration represents the concrete implementation of BankCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package bankuc

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
	"gitlab.com/zenpagos/will/usecase"
)

type BankUseCase struct {
	log            *logrus.Logger
	BankRepository repository.BankRepositoryInterface
}

func NewBankUseCase(
	log *logrus.Logger,
	br repository.BankRepositoryInterface,
) usecase.BankUseCaseInterface {
	return BankUseCase{
		log:            log,
		BankRepository: br,
	}
}

func (uc BankUseCase) CreateBank(b *model.Bank) error {
	if err := b.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := b.Complete(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := uc.BankRepository.Insert(b); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc BankUseCase) ListBanks() ([]model.Bank, error) {
	return uc.BankRepository.FindAll()
}
