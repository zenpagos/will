// Package registration represents the concrete implementation of BrandCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package branduc

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
	"gitlab.com/zenpagos/will/usecase"
)

type BrandUseCase struct {
	log             *logrus.Logger
	BrandRepository repository.BrandRepositoryInterface
}

func NewBrandUseCase(
	log *logrus.Logger,
	br repository.BrandRepositoryInterface,
) usecase.BrandUseCaseInterface {
	return BrandUseCase{
		log:             log,
		BrandRepository: br,
	}
}

func (uc BrandUseCase) CreateBrand(b *model.Brand) error {
	if err := b.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := b.Complete(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := uc.BrandRepository.Insert(b); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc BrandUseCase) ListBrands() ([]model.Brand, error) {
	return uc.BrandRepository.FindAll()
}
