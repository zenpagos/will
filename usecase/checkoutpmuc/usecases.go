// Package registration represents the concrete implementation of PaymentMethodFeeCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package checkoutpmuc

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/will/config"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/usecase"
)

type CheckoutPaymentMethodUseCase struct {
	log           *logrus.Logger
	Configuration config.Configuration
}

func NewCheckoutPaymentMethodUseCase(
	log *logrus.Logger,
	conf config.Configuration,
) usecase.CheckoutPaymentMethodUseCaseInterface {
	return CheckoutPaymentMethodUseCase{
		log:           log,
		Configuration: conf,
	}
}

func (uc CheckoutPaymentMethodUseCase) BuildCheckoutPaymentMethod(
	paymentMethod *model.PaymentMethod,
	amount int64,
) (*model.CheckoutPaymentMethod, error) {

	cis := make([]*model.CheckoutInstallment, 0)
	for _, fee := range paymentMethod.Fees {
		amountToPay := int64(float32(amount) * fee.CardCoefficient)
		installmentAmount := amountToPay / fee.Installments

		cis = append(cis, &model.CheckoutInstallment{
			Installments:      fee.Installments,
			InstallmentAmount: installmentAmount,
			AmountToPay:       amountToPay,
		})
	}

	cpm := &model.CheckoutPaymentMethod{
		Name:         paymentMethod.Brand.CanonicalName,
		Type:         paymentMethod.Type,
		LogoURL:      paymentMethod.Brand.LogoURL,
		Installments: cis,
	}

	return cpm, nil
}
