// Package registration represents the concrete implementation of PaymentMethodFeeCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package feeuc

import (
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
	"gitlab.com/zenpagos/will/usecase"
)

type FeeUseCase struct {
	log           *logrus.Logger
	FeeRepository repository.FeeRepositoryInterface
}

func NewFeeUseCase(
	log *logrus.Logger,
	pmr repository.FeeRepositoryInterface,
) usecase.FeeUseCaseInterface {
	return FeeUseCase{
		log:           log,
		FeeRepository: pmr,
	}
}

func (uc FeeUseCase) CreateFee(f *model.Fee) error {
	if err := f.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := f.Complete(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := uc.FeeRepository.Insert(f); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc FeeUseCase) ListFees() ([]model.Fee, error) {
	return uc.FeeRepository.FindAll()
}

func (uc FeeUseCase) GetFeeToExecuteAPayment(
	accountID uuid.UUID,
	paymentMethodType string,
	paymentMethod string,
	installments int64,
) (*model.Fee, error) {
	return uc.FeeRepository.FindOneByPaymentInformation(
		accountID,
		paymentMethodType,
		paymentMethod,
		installments,
	)
}
