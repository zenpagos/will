// Package registration represents the concrete implementation of MerchantCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package merchantuc

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
	"gitlab.com/zenpagos/will/usecase"
)

type MerchantUseCase struct {
	log                *logrus.Logger
	MerchantRepository repository.MerchantRepositoryInterface
}

func NewMerchantUseCase(
	log *logrus.Logger,
	mr repository.MerchantRepositoryInterface,
) usecase.MerchantUseCaseInterface {
	return MerchantUseCase{
		log:                log,
		MerchantRepository: mr,
	}
}

func (uc MerchantUseCase) CreateMerchant(m *model.Merchant) error {
	if err := m.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := uc.MerchantRepository.Insert(m); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc MerchantUseCase) ListMerchants() ([]model.Merchant, error) {
	return uc.MerchantRepository.FindAll()
}
