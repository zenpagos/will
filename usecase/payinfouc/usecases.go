// Package registration represents the concrete implementation of PaymentMethodFeeCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package payinfouc

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/tools"
	"gitlab.com/zenpagos/will/config"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/usecase"
	"time"
)

type PaymentInformationUseCase struct {
	log           *logrus.Logger
	Configuration config.Configuration
}

func NewPaymentInformationUseCase(
	log *logrus.Logger,
	conf config.Configuration,
) usecase.PaymentInformationUseCaseInterface {
	return PaymentInformationUseCase{
		log:           log,
		Configuration: conf,
	}
}

func (uc PaymentInformationUseCase) BuildPaymentInformation(
	account *model.Account,
	fee *model.Fee,
	amount int64,
) (*model.PaymentInformation, error) {

	pi := &model.PaymentInformation{
		Description:               fee.Description,
		CardCoefficient:           fee.CardCoefficient,
		ProcessingCostPercentage:  fee.ProcessingCostPercentage,
		ProcessingCostFixedAmount: fee.ProcessingCostFixedAmount,
		FeePercentage:             fee.FeePercentage,
		FeeFixedAmount:            fee.FeeFixedAmount,
		DaysOnHold:                fee.DaysOnHold,
		Installments:              fee.Installments,
		Amount:                    amount,
		TaxRate:                   uc.Configuration.Service.DefaultTaxRate,
		StatementDescriptor:       tools.StringValueOrBlank(account.StatementDescriptor),

		// decidir information
		DecidirPrivateKey: fee.PaymentMethod.Merchant.DecidirPrivateKey,

		// merchant information
		MerchantTIN:            fee.PaymentMethod.Merchant.TIN,
		MerchantTINType:        fee.PaymentMethod.Merchant.TINType,
		MerchantName:           fee.PaymentMethod.Merchant.Name,
		MerchantStreet:         fee.PaymentMethod.Merchant.Street,
		MerchantDoorNumber:     fee.PaymentMethod.Merchant.DoorNumber,
		MerchantZipCode:        fee.PaymentMethod.Merchant.ZipCode,
		MerchantCategory:       fee.PaymentMethod.Merchant.Category,
		MerchantGeographicCode: fee.PaymentMethod.Merchant.GeographicCode,
		MerchantCity:           fee.PaymentMethod.Merchant.City,
		MerchantID:             fee.PaymentMethod.Merchant.DecidirMerchantID,
		MerchantProvince:       fee.PaymentMethod.Merchant.Province,
		MerchantCountry:        fee.PaymentMethod.Merchant.Country,
		MerchantEmail:          fee.PaymentMethod.Merchant.Email,
		MerchantPhone:          fee.PaymentMethod.Merchant.Phone,
	}

	pi.AmountToPay = int64(float32(pi.Amount) * pi.CardCoefficient)
	pi.InstallmentAmount = pi.AmountToPay / pi.Installments
	pi.FeeAmount = int64(float32(pi.Amount)*pi.FeePercentage/100) + pi.FeeFixedAmount
	pi.TaxesAmount = int64(float32(pi.FeeAmount) * pi.TaxRate / 100)
	pi.ReceivedAmount = pi.Amount - (pi.FeeAmount + pi.TaxesAmount)
	pi.AmountReleaseDate = time.Now().UTC().AddDate(0, 0, int(pi.DaysOnHold)).Unix()

	return pi, nil
}
