// Package registration represents the concrete implementation of PaymentMethodCaseInterface interface.
// Because the same business function can be created to support both transaction and non-transaction,
// a shared business function is created in a helper file, then we can wrap that function with transaction
// or non-transaction.
package pmuc

import (
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/zenpagos/will/model"
	"gitlab.com/zenpagos/will/repository"
	"gitlab.com/zenpagos/will/usecase"
)

type PaymentMethodUseCase struct {
	log                     *logrus.Logger
	PaymentMethodRepository repository.PaymentMethodRepositoryInterface
}

func NewPaymentMethodUseCase(
	log *logrus.Logger,
	br repository.PaymentMethodRepositoryInterface,
) usecase.PaymentMethodUseCaseInterface {
	return PaymentMethodUseCase{
		log:                     log,
		PaymentMethodRepository: br,
	}
}

func (uc PaymentMethodUseCase) CreatePaymentMethod(pm *model.PaymentMethod) error {
	if err := pm.Validate(); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	if err := uc.PaymentMethodRepository.Insert(pm); err != nil {
		uc.log.WithError(err).Error()
		return err
	}

	return nil
}

func (uc PaymentMethodUseCase) ListPaymentMethods() ([]model.PaymentMethod, error) {
	return uc.PaymentMethodRepository.FindAll()
}

func (uc PaymentMethodUseCase) GetPaymentMethod(id uuid.UUID) (*model.PaymentMethod, error) {
	return uc.PaymentMethodRepository.FindOne(id)
}

func (uc PaymentMethodUseCase) GetPaymentMethodForCheckout(
	accountID uuid.UUID,
	paymentMethod string,
	paymentMethodType string,
) (*model.PaymentMethod, error) {
	return uc.PaymentMethodRepository.FindOneByAccountIDNameAndType(accountID, paymentMethod, paymentMethodType)
}
