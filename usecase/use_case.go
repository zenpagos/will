// Package usecase defines all the interfaces for a Micro-service application.
// It is the entry point for the application's business logic. It is a top level package for a Micro-service application.
// This top level package only defines interface, the concrete implementations are defined in sub-package of it.
// It only depends on model package. No other package should dependent on it except cmd.

// If transaction is supported, the transaction boundary should be defined in this package.
// A suffix-"WithTx" can be added to the name of a transaction function to distinguish it from a non-transaction one.
package usecase

import (
	uuid "github.com/satori/go.uuid"
	"gitlab.com/zenpagos/will/model"
)

type MerchantUseCaseInterface interface {
	CreateMerchant(merchant *model.Merchant) error
	ListMerchants() ([]model.Merchant, error)
}

type BrandUseCaseInterface interface {
	CreateBrand(brand *model.Brand) error
	ListBrands() ([]model.Brand, error)
}

type BankUseCaseInterface interface {
	CreateBank(bank *model.Bank) error
	ListBanks() ([]model.Bank, error)
}

type PaymentMethodUseCaseInterface interface {
	CreatePaymentMethod(paymentMethod *model.PaymentMethod) error
	ListPaymentMethods() ([]model.PaymentMethod, error)
	GetPaymentMethod(id uuid.UUID) (*model.PaymentMethod, error)
	GetPaymentMethodForCheckout(
		accountID uuid.UUID,
		paymentMethod string,
		paymentMethodType string,
	) (*model.PaymentMethod, error)
}

type FeeUseCaseInterface interface {
	CreateFee(fee *model.Fee) error
	ListFees() ([]model.Fee, error)
	GetFeeToExecuteAPayment(
		accountID uuid.UUID,
		paymentMethodType string,
		paymentMethod string,
		installments int64,
	) (*model.Fee, error)
}

type PaymentInformationUseCaseInterface interface {
	BuildPaymentInformation(
		account *model.Account,
		fee *model.Fee,
		amount int64,
	) (*model.PaymentInformation, error)
}

type CheckoutPaymentMethodUseCaseInterface interface {
	BuildCheckoutPaymentMethod(
		paymentMethod *model.PaymentMethod,
		amount int64,
	) (*model.CheckoutPaymentMethod, error)
}

type AccountUseCaseInterface interface {
	CreateAccount(account *model.Account) error
	ListAccounts() ([]model.Account, error)
	GetAccountDetail(id uuid.UUID) (*model.Account, error)
}

type BankAccountUseCaseInterface interface {
	CreateBankAccount(bankAccount *model.BankAccount) error
	ListBankAccounts() ([]model.BankAccount, error)
}
