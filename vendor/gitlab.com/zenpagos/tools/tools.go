package tools

import (
	"github.com/speps/go-hashids"
	"time"
)

func NewBoolPointer(b bool) *bool {
	return &b
}

func NewStringPointer(s string) *string {
	return &s
}

func StringPointerOrNil(s string) *string {
	if s == "" {
		return nil
	}
	return &s
}

func StringValueOrBlank(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}

func Int64PointerOrNil(i int64) *int64 {
	if i == 0 {
		return nil
	}
	return &i
}

func Int64ValueOrZero(i *int64) int64 {
	if i == nil {
		return 0
	}
	return *i
}

func NewIntPointer(i int) *int {
	return &i
}

func NewInt32Pointer(i32 int32) *int32 {
	return &i32
}

func NewInt64Pointer(i64 int64) *int64 {
	return &i64
}

func NewUintPointer(i uint) *uint {
	return &i
}

func NewUint32Pointer(ui32 uint32) *uint32 {
	return &ui32
}

func NeUint64Pointer(ui64 uint64) *uint64 {
	return &ui64
}

func NewUnixTime() int64 {
	return time.Now().UTC().Unix()
}

func NewUnixTimePointer() *int64 {
	n := time.Now().UTC().Unix()
	return &n
}

func NextUnixTime(d int64) int64 {
	return time.Now().UTC().AddDate(0, 0, int(d)).Unix()
}

func NextUnixTimePointer(d *int64) *int64 {
	next := time.Now().UTC().AddDate(0, 0, int(*d)).Unix()
	return &next
}

func NewShortID() string {
	now := NewUnixTime()
	hd := hashids.NewData()
	h, _ := hashids.NewWithData(hd)
	id, _ := h.Encode([]int{int(now)})

	return id
}
